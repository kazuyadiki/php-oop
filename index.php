<?php
require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$sheep = new Animal("Shaun");

echo "Nama Hewan = " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki = " . $sheep->legs . "<br>"; // 2
echo "Hewan Berdarah Dingin = " . $sheep->cold_blooded . "<br><br>";// false

$sungokong = new Ape("Kera Sakti");

echo "Nama Hewan = " . $sungokong->name . "<br>";
echo "Jumlah Kaki = " . $sungokong->legs . "<br>";
echo "Hewan Berdarah Dingin = " . $sungokong->cold_blooded . "<br>";
$sungokong->yell();

echo "<br><br>";

$kodok = new Frog("Buduk");

echo "Nama Hewan = " . $kodok->name . "<br>";
echo "Jumlah Kaki = " . $kodok->legs . "<br>";
echo "Hewan Berdarah Dingin = " . $kodok->cold_blooded . "<br>";
$kodok->jump();

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>